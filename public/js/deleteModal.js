/**
 * deleteModal.js
 */
 window.onload = function() {

    // Ecouteur d'évènement sur les boutons de suppression
    document.querySelectorAll('.confirm-delete').forEach(button => {
        button.addEventListener('click', function() {
            let id = this.dataset.id;
            let href = `/account/delete/picture/${id}`;

            let modal = document.querySelector('#staticBackdrop');
            let button = modal.querySelector('.btn-danger');
            button.href = href;

            var myModal = new bootstrap.Modal(modal);
            myModal.show();
        });
    });
}