<?php

namespace App\Controller;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\User;
use App\Entity\Picture;
use App\Form\EditPictureType;
use App\Form\UploadPictureType;

class AccountController extends AbstractController
{
    /**
     * @Route("/account", name="account")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        // $this->getUser() est l'équivalent de "app.user"
        $resultPictures = $this->getDoctrine()->getRepository(Picture::class)->findBy(['user' => $this->getUser()]);
        // Page par page
        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            $resultPictures, // Requête contenant les données à paginer
            // $this->getUser()->getPictures()
            $page === 0 ? 1 : $page, // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            4 // Nombre de résultats par page
        );
        return $this->render('account/index.html.twig', [
            'pictures' => $pictures
        ]);
    }

    /**
     * @Route("/account/new/picture", name="account_new_picture")
     */
    public function new(Request $request) {
        // On instancie l'entité Picture
        $picture = new Picture();

        $formUpload = $this->createForm(UploadPictureType::class, $picture);

        // Insersion dans BDD
        $formUpload->handleRequest($request);
        // Vérifie si le formulaire est envoyé et valide
        if ($formUpload->isSubmitted() && $formUpload->isValid()) {
            // On insère la date
            $picture->setCreatedAt(new \DateTimeImmutable());
            // On insère l'utilisateur actuellement connecté
            $picture->setUser($this->getUser());            
            // Insersion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($picture);
            $doctrine->flush();
            // Message flash
            $this->addFlash('success', 'Merci, l\'image bien a été partagée !' );
            // Redirection vers la page d'accueil
            return $this->redirectToRoute('account');
        }

        return $this->render('account/new.html.twig', [
            'formUpload' => $formUpload->createView()
        ]);
    }

    /**
     * @Route("/account/edit/picture/{id}", name="account_edit_picture")
     */
    public function edit($id, Request $request) {
        // On sélectionne un enregistrement en BDD
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        $formEdit = $this->createForm(EditPictureType::class, $picture);

        // Insersion dans BDD
        $formEdit->handleRequest($request);
        // Vérifie si le formulaire est envoyé et valide
        if ($formEdit->isSubmitted() && $formEdit->isValid()) {
            // On insère la date
            $picture->setUpdatedAt(new \DateTimeImmutable());
               
            // Insersion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($picture);
            $doctrine->flush();
            // Message flash
            $this->addFlash('success', 'Merci, l\'image bien a été modifiée !' );
            // Redirection vers la page d'accueil
            return $this->redirectToRoute('account');
        }
        

        return $this->render('account/edit.html.twig', [
            'formEdit' => $formEdit->createView()
        ]);
    }


    /**
     * @Route("/account/delete/picture/{id}", name="account_delete_picture")
     */
    public function delete($id) {

        // On sélectionne un enregistrement en BDD
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        // On supprime l'image de la BDD
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($picture);
        $manager->flush();

        // Message flash
        $this->addFlash('success', 'Merci, l\'image bien a été supprimée !' );
        // Redirection vers la page d'accueil
        return $this->redirectToRoute('account');
    }

}
