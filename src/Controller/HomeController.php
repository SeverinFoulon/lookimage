<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Picture;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request; // Nous avons besoin d'accéder à la requête pour obtenir le numéro de page
use Knp\Component\Pager\PaginatorInterface; // Nous appelons le bundle KNP Paginator

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        // Méthode findBy qui permet de récupérer les données avec des critères de filtre et de tri
        $resultPictures = $this->getDoctrine()->getRepository(Picture::class)->findBy([], ['created_at' => 'desc']);

        // Méthode findAll qui permet de récupérer toutes les données
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        // Page par page
        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            $resultPictures, // Requête contenant les données à paginer
            $page === 0 ? 1 : $page, // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            6 // Nombre de résultats par page
        );

        return $this->render('home/index.html.twig', [
            'pictures' => $pictures,
            'categories' => $categories,
            'resultPictures' => $resultPictures
        ]);
    }

    /**
     * @Route("/category/{id}", name="pictures_by_category")
     */
    public function picturesByCategory($id, Request $request, PaginatorInterface $paginator)
    {
        // dd($id); équivalent de var_dump()

        // On récupère la catégorie suivant l'id (seul l'id fonctionne avec le find())
        $categorie = $this->getDoctrine()->getRepository(Categorie::class)->find($id);
        // Si la catégorie n'existe pas, erreur 404
        if (!$categorie) {
            throw $this->createNotFoundException('La catégorie n\'éxiste pas !');
        }

        // Page par page
        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            $categorie->getPictures(),
            $page === 0 ? 1 : $page,
            6
        );

        return $this->render('home/picturesByCategory.html.twig', [
            'categorie' => $categorie,
            'pictures' => $pictures
        ]);
    }

    /**
     * @Route("/category/picture/{id}", name="picture")
     */
    public function picture($id, Request $request, PaginatorInterface $paginator)
    {
        // On récupère l'image suivant l'id
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);
        if (!$picture) {
            throw $this->createNotFoundException('Cette image n\'éxiste pas !');
        }

        // Page par page
        $page = $request->query->getInt('page', 1);
        $photos = $paginator->paginate(
            $picture->getCategory()->getPictures(),
            $page === 0 ? 1 : $page,
            3
        );

        return $this->render('home/picture.html.twig', [
            'picture' => $picture,
            'photos' => $photos
        ]);
    }


}
