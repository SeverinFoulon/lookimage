<?php

namespace App\Form;

use App\Entity\Categorie;
use App\Entity\Picture;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Image;

class EditPictureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('imageFile', VichImageType::class, [
                'required' => false,
                'label' => 'Sélectionner une image',
                // Options de VichImageType
                'allow_delete' => false,
                'download_uri' => false,
                'imagine_pattern' => 'thumbnail',
                'constraints' => [
                    // new NotBlank([
                    //     'message' => 'Veuillez choisir une image !'
                    // ]),
                    new Image([
                        'maxSize' => '2M',
                        'maxSizeMessage' => 'Le fichier ne doit pas dépasser 2Mo !',
                        'mimeTypes' => [
                            'image/gif', 'image/png', 'image/jpeg', 'image/webp'
                        ],
                        'mimeTypesMessage' => 'Cette image est invalide !'
                    ])
                ]
            ])
            ->add('description', TextareaType::class, [
                'required' => true,
                'label' => 'Description',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir une description !'
                    ])
                ]
            ])
            ->add('tags', TextType::class, [
                'required' => true,
                'label' => 'Tags',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir un ou plusieurs tags !'
                    ])
                ]
            ])
            ->add('category', EntityType::class, [
                'required' => true,
                'label' => 'Catégorie de l\'image',
                'class' => Categorie::class,
                'choice_label' => 'name'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Picture::class,
        ]);
    }
}
