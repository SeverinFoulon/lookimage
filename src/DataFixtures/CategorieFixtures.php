<?php

namespace App\DataFixtures;

use App\Entity\Categorie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
// Faker
use Faker;
use Symfony\Component\String\Slugger\AsciiSlugger;

class CategorieFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Instancier Faker
        $faker = Faker\Factory::create('fr_FR');

        // Création d'une boucle pour choisir le nombre d'éléments allant en BDD
        for($i = 0; $i <= 10; $i++) {
            $category = new Categorie();
            $category->setName($faker->colorName);
            $category->setSlug((new AsciiSlugger())->slug(strtolower($faker->colorName)));

            // Enregistre l'objet dans une référence, pour l'utiliser dans une autre fixture (relation de table)
            // Le premier paramètre est un nom unique, le second paramètre est l'objet lié à ce nom
            $this->addReference('category_'. $i, $category);

            // Garde de coté en attendant l'éxécution des requètes
            $manager->persist($category);
        }

        $manager->flush();
    }
}
