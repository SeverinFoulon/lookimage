<?php

namespace App\DataFixtures;
// Picture
use App\Entity\Picture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Mmo\Faker\PicsumProvider;
// Faker
use Faker;
use Symfony\Component\HttpFoundation\File\File;

class PictureFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies() {
        return [
            CategorieFixtures::class,
            UserFixtures::class
        ];
    }

    public function load(ObjectManager $manager)
    {
        // Instancier Faker
        $faker = Faker\Factory::create('fr_FR');

        // On ajoute le bundle "Mmo Faker-image" à faker
        $faker->addProvider(new PicsumProvider($faker));

        // Création d'une boucle pour choisir le nombre d'éléments allant en BDD
        for($i = 0; $i <= 50; $i++) {

            // Télécharger une image (chemin, largeur, hauteur)
            $image = $faker->picsum('./public/upload/images/photos', random_int(1152, 2312), random_int(864, 1736));

            // Récupération d'une référence aléatoirement, on récupère un objet de l'entité Categorie généré dans le fichier CategorieFixtures, grâce au nom choisi lors de l'enregistrement dans les références
            $category = $this->getReference('category_'. random_int(0, 10));

            // Récupération d'une référence utilisateur aléatoirement
            $user = $this->getReference('user_'. random_int(0, 10));

            $picture = new Picture();
            $picture->setDescription($faker->sentence(30));
            $picture->setTags($faker->word);
            $picture->setCreatedAt($faker->dateTimeBetween('- 4 years', '- 2 years'));
            $picture->setUpdatedAt($faker->dateTimeBetween('- 2 years'));
            $picture->setCategory($category);
            $picture->setUser($user);

            // Gestion de l'image
            $picture->setImageFile(new File($image));
            // str_replace (permet de rechercher un morceau de caractères, et le remplacer, dans une chaine de caractères)
            $picture->setImage(str_replace('./public/upload/images/photos/', '', $image));

            
            // Garde de coté en attendant l'éxécution des requètes
            $manager->persist($picture);
        }

        $manager->flush();
    }
}
